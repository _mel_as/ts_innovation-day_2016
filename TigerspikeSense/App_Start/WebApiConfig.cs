﻿using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using TigerspikeSense.Controllers;
using TigerspikeSense.Interfaces;
using TigerspikeSense.Services;

namespace TigerspikeSense
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var builder = new ContainerBuilder();
            builder.Register(d => new DashboardController(d.Resolve<ICalendarService>(), d.Resolve<IProjectService>()));
            builder.RegisterType<CalendarService>().As<ICalendarService>();
            builder.RegisterType<ProjectService>().As<IProjectService>();

            var container = builder.Build();
            var resolver = new AutofacWebApiDependencyResolver(container);
            config.DependencyResolver = resolver;

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
