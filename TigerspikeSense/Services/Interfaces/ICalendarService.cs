﻿using System.Collections.Generic;
using TigerspikeSense.Models;

namespace TigerspikeSense.Interfaces
{
    public interface ICalendarService
    {
        List<CalendarAlertsModel> GetCalendarAlerts();

        List<RoomAvailiabilityModel> GetMeetingRoomAvailability();
    }
}
