﻿using System.Collections.Generic;
using TigerspikeSense.Models;

namespace TigerspikeSense.Interfaces
{
    public interface IProjectService
    {
        List<ProjectWipModel> GetProjectStatusList();
    }
}
