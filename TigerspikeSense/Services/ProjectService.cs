﻿using System.Collections.Generic;
using TigerspikeSense.Interfaces;
using TigerspikeSense.Models;

namespace TigerspikeSense.Services
{
    public class ProjectService : IProjectService
    {
        public List<ProjectWipModel> GetProjectStatusList()
        {
            return RetriveProjects();
        }

        private List<ProjectWipModel> RetriveProjects()
        {
            return new List<ProjectWipModel>()
            {
                new ProjectWipModel() {
                    ProjectName ="Axicorp",
                    PrCode ="PR13644",
                    Status = ProjectStatus.HIGHRISK,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="FACS Housing",
                    PrCode ="PR13709",
                    Status = ProjectStatus.LOWRISK,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="GTSE Define & Build",
                    PrCode ="PR13515",
                    Status = ProjectStatus.LOWRISK,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="SNSW Digital Licence",
                    PrCode ="PR13611",
                    Status = ProjectStatus.LOWRISK,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="Medibank Carekit MVP",
                    PrCode ="PR13544",
                    Status = ProjectStatus.LOWRISK,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="NBN 1580",
                    PrCode ="PR13688",
                    Status = ProjectStatus.LOWRISK,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="SNSW DL 1.1b",
                    PrCode ="PR13703",
                    Status = ProjectStatus.LOWRISK,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="Magic Memories MVP",
                    PrCode ="PR13604",
                    Status = ProjectStatus.LOWRISK,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="FACS Custom Console",
                    PrCode ="PR13493",
                    Status = ProjectStatus.OK,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="Asciano My Train XD",
                    PrCode ="PR13588",
                    Status = ProjectStatus.OK,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="Rural Co",
                    PrCode ="PR13663",
                    Status = ProjectStatus.OK,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="GI Hub",
                    PrCode ="PR13624",
                    Status = ProjectStatus.OK,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="CIMIC",
                    PrCode ="PR13665",
                    Status = ProjectStatus.OK,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="Tradie XD",
                    PrCode ="PR13701",
                    Status = ProjectStatus.OK,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="NBN 2291 Dev",
                    PrCode ="PR13589",
                    Status = ProjectStatus.OK,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="Chamberlain XD",
                    PrCode ="PR13534",
                    Status = ProjectStatus.OK,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="GrainCorp Marketplace",
                    PrCode ="PR13300",
                    Status = ProjectStatus.OK,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="Mirvac Residential",
                    PrCode ="PR13373",
                    Status = ProjectStatus.HOLD,
                    Type = ProjectType.EXTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="Phoenix Intelligence",
                    PrCode ="PR10104",
                    Status = ProjectStatus.LOWRISK,
                    Type = ProjectType.INTERNAL
                },
                new ProjectWipModel() {
                    ProjectName ="Pardot",
                    PrCode ="PR13138",
                    Status = ProjectStatus.OK,
                    Type = ProjectType.INTERNAL
                }
            };
        }
    }
}