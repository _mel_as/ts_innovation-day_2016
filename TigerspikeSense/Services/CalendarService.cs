﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Web.Configuration;
using TigerspikeSense.Interfaces;
using TigerspikeSense.Models;

namespace TigerspikeSense.Services
{
    public class CalendarService : ICalendarService
    {
        // If modifying these scopes, delete your previously saved credentials
        // at ~/.credentials/calendar-dotnet-quickstart.json
        static string[] Scopes = { Google.Apis.Calendar.v3.CalendarService.Scope.CalendarReadonly };
        static string ApplicationName = "Google Calendar API .NET Quickstart";
        static string SydneyLeaveCalendar = "tigerspike.com_fpkbtpup02iuahmm8vb403r7r0@group.calendar.google.com";
        static string CalendarAuthenticationToken = WebConfigurationManager.AppSettings["GoogleAuth"];
        UserCredential credential = GetCredential();
        static string[] MeetingRoomCalendars = {
            "tigerspike.com_2d3631313633393534333233@resource.calendar.google.com",
            "tigerspike.com_333532323730362d373030@resource.calendar.google.com",
            "tigerspike.com_2d3436313732313636323836@resource.calendar.google.com"
        };
        static string[] MeetingRoomNames = {
            "Shinjuku",
            "Tanjong Pagar",
            "Warsaw"
        };

        public List<RoomAvailiabilityModel> GetMeetingRoomAvailability()
        {
            var roomList = new List<RoomAvailiabilityModel>();
            var hack = 0;
            foreach (var cal in MeetingRoomCalendars)
            {
                var request = GetCalendar(cal);
                request.TimeMin = DateTime.Now;
                request.ShowDeleted = false;
                request.SingleEvents = true;
                request.MaxResults = 10;
                request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

                var meetingRoom = new RoomAvailiabilityModel()
                {
                    Name = MeetingRoomNames[hack],
                    IsInUse = "VACANT"
                };

                Events events = request.Execute();
                if (events.Items != null && events.Items.Count > 0)
                {
                    foreach (var eventItem in events.Items)
                    {
                        //var d = new DateTime(2016, 11, 25, 9, 15, 0);
                        //if (eventItem.Start.DateTime < d && eventItem.End.DateTime > d)
                        if (eventItem.Start.DateTime < DateTime.Now && eventItem.End.DateTime > DateTime.Now)
                        {
                            meetingRoom.IsInUse = "IN USE";
                        }
                    }
                }
                roomList.Add(meetingRoom);
                hack++;
            }

            return roomList;
        }

        public List<CalendarAlertsModel> GetCalendarAlerts()
        {
            var calendarAlerts = new List<CalendarAlertsModel>();
            var request = GetCalendar(SydneyLeaveCalendar);

                request.TimeMin = DateTime.Now;
                request.ShowDeleted = false;
                request.SingleEvents = true;
                request.MaxResults = 10;
                request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

                // List events.
                Events events = request.Execute();
            if (events.Items != null && events.Items.Count > 0)
            {
                foreach (var eventItem in events.Items)
                {
                    var start = FormatDate(eventItem.Start.Date);
                    var end = FormatDate(eventItem.End.Date);
                    if (start < DateTime.Today && end > DateTime.Today)
                    {
                        calendarAlerts.Add(new CalendarAlertsModel()
                        {
                            Summary = eventItem.Summary,
                            Date = string.Format("{0} to {1}", start.ToShortDateString(), end.ToShortDateString())
                        });
                    }
                }
            }
            else
            {
                calendarAlerts.Add(new CalendarAlertsModel()
                {
                    Date = string.Format("No upcoming events found.")
                });
            }
            return calendarAlerts;
        }

        private DateTime FormatDate(string date)
        {
            string[] d = date.Split('-');
            return new DateTime(Convert.ToInt32(d[0]), Convert.ToInt32(d[1]), Convert.ToInt32(d[2]));
        }

        private EventsResource.ListRequest GetCalendar(string calendar)
        {
            // Create Google Calendar API service.
            var service = new Google.Apis.Calendar.v3.CalendarService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            // Define parameters of request.
            EventsResource.ListRequest request = service.Events.List(calendar);

            return request;
        }

        private static UserCredential GetCredential()
        {
            UserCredential credential;
            using (var stream = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, CalendarAuthenticationToken), FileMode.Open, FileAccess.Read))
            {
                string credPath = System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(credPath, ".credentials/calendar-dotnet-quickstart.json");

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }

            return credential;
        }
    }
}