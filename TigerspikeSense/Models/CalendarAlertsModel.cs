﻿using System.Collections.Generic;

namespace TigerspikeSense.Models
{
    public class CalendarAlertsModel
    {
        public string Summary { get; set; }
        public string Date { get; set; } 
    }
}