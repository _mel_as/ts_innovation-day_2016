﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TigerspikeSense.Models
{
    public class RoomAvailiabilityModel
    {
        public string Name { get; set; }
        public string IsInUse { get; set; }
    }
}