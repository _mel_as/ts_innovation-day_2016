﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TigerspikeSense.Models
{
    public class ProjectWipModel
    {
        public string ProjectName { get; set; }
        public string PrCode { get; set; }
        public ProjectStatus Status { get; set; }
        public ProjectType Type { get; set; }

    }

    public enum ProjectStatus
    {
        OK = 0,
        LOWRISK = 1,
        HIGHRISK = 2,
        HOLD = 3
    }

    public enum ProjectType
    {
        EXTERNAL = 0,
        INTERNAL = 1
    }
}