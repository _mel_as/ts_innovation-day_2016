﻿using System.Collections.Generic;
using System.Web.Http;
using TigerspikeSense.Interfaces;
using TigerspikeSense.Models;

namespace TigerspikeSense.Controllers
{
    public class DashboardController : ApiController
    {
        private readonly ICalendarService _calendarService;
        private readonly IProjectService _projectService;

        public DashboardController() { }

        public DashboardController(ICalendarService calendarService, IProjectService projectService)
        {
            _calendarService = calendarService;
            _projectService = projectService;
        }

        public List<CalendarAlertsModel> GetCalendarAlerts()
        {
            return _calendarService.GetCalendarAlerts();
        }

        public List<RoomAvailiabilityModel> GetMeetingRoomAvailability()
        {
            return _calendarService.GetMeetingRoomAvailability();
        }

        public List<ProjectWipModel> GetProjectStatusList()
        {
            return _projectService.GetProjectStatusList();
        }

    }
}