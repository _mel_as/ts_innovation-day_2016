echo "" > nowplaying
old=""
while true
do
   read f < nowplaying
   if [ "$old" != "$f" ]; then
      echo $f
      /usr/local/bin/spotify play "$f"
      old=$f
   fi
done
