//
//  ViewController.swift
//  SlackToFirebase
//
//  Created by Max Chuquimia on 24/11/2016.
//  Copyright © 2016 Chuquimian Productions. All rights reserved.
//

import UIKit
import Foundation
import SlackKit
import Firebase
import FirebaseCore
import FirebaseDatabase
import Darwin

class ViewController: UIViewController {
    
    //CP Scripts
    //let bot = SlackKit(withAPIToken: "xoxb-108791629316-mjbYki8q38m7JrMJp2KoUMbO")

    //TIGERSPIKE DASH
    let bot = SlackKit(withAPIToken: "xoxb-109615173463-vPh8IRvsFg0zHVIDTQ7fS9zS")

    var db: FIRDatabaseReference!
    var data: [[String: String]] = [
        ["user": "Tigerspike Dash", "image": "https://www.dreamhire.io/img/9558fa15-064b-4a98-b2a1-a5a91eeb0d40", "message": "Welcome to Tigerspike Dash! Messages posted on #sydney will appear here"]
    ]
    let json = try! JSONSerialization.jsonObject(with: Data(contentsOf: URL(fileURLWithPath:Bundle.main.path(forResource: "users", ofType: "json")!), options: []), options: []) as! [String: Any]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        db = FIRDatabase.database().reference(withPath: "/data")
        //Tigerspike
        //let bot = SlackKit(withAPIToken: "")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1000, execute: {
            self.db.setValue(self.data)
        })

        self.bot.onClientInitalization = { (client) in
            DispatchQueue.main.async(execute: {
                client.messageEventsDelegate = self
            })
        }
    }
}

extension ViewController: MessageEventsDelegate {
    func sent(_ message: Message, client: Client) {
        print("SLACK", message, client)
    }
    
    func received(_ message: Message, client: Client) {
        print("SLACK",message, client)
        
        let content = message.text ?? "Error 1"
        var user = message.user ?? "Error 2"
        var url = message.icons?.first?.value ?? "Error 3"
        
        print(content, user, message.icons ?? "")
        
        let members = self.json["members"] as! [[String: Any]]
        
        let thisMemberIndex = members.index { (obj) -> Bool in
            return (obj["id"] as! String) == user
        }
        
        //dump(message)
        
        if let idx = thisMemberIndex {
            let m = members[idx]
            user = (m["profile"] as? [String: Any])?["real_name_normalized"] as? String ?? "Error 4"
            url = (m["profile"] as? [String: Any])?["image_72"] as? String ?? "Error 5"
        }
        
        let data = [
            "message": content,
            "image": url,
            "user": user,
            "preview": message.attachments?.first?.imageURL ?? ""
        ]
        
        self.data.append(data as! [String : String])
        
        db.setValue(self.data)
        
        if content.hasPrefix("<@U37J353DM> play") {
            let term = content.replacingOccurrences(of: "<@U37J353DM> play ", with: "")
            print(term)
            
            let data = term.data(using: .utf8)!
            try! data.write(to: URL(fileURLWithPath: "/Users/max.chuquimia/Desktop/projects/InnovationDay/SYD-Tigerspike_Dash/nowplaying"))
            
            //play(term)
        }
    }
    
    func changed(_ message: Message, client: Client) {
        print("SLACK",message, client)
    }
    
    func deleted(_ message: Message?, client: Client) {
        print("SLACK",message ?? "-", client)
    }
    
    func play(_ args: String...) {
        let task = NSTask()!
        task.setLaunchPath("/usr/local/bin/spotify")
        task.setArguments(args)
        task.launch()
        print(task, task.arguments(), task.launchPath())
        task.waitUntilExit()
    }
}

